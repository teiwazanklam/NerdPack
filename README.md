

<h2 align="center"><b>NerdPack engine for World of Warcraft.</b></h2>

---------------------------------------------------------------
<br/>

<p><b>NerdPack</b> is a engine/botbase/framework wich aims to be light and allow creating combat routines easly.</p>
<p><b>Donate Link</b>: http://goo.gl/yrctPO</p>
<br/>
<h2>Modules:</h2>
* <b>Combat Routines</b>: https://gitlab.com/MrTheSoulz/NerdPack-Routines
* <b>Fishing Bot</b>: https://gitlab.com/MrTheSoulz/NerdPack-FSH
* <b>Gathering Bot</b>: https://gitlab.com/MrTheSoulz/NerdPack-GB
* <b>Overlays</b>: https://gitlab.com/MrTheSoulz/NerdPack-Overlays
* <b>BattlePet Bot</b>: https://gitlab.com/MrTheSoulz/NerdPack-PB
<br/>
---------------------------------------------------------------
<h2>Instructions on how to install:</h2>
* Close WoW before updating/installing.
* Extract NerdPack and move it to: "wow\Interface\Addons" folder.
* Rename the folder name to "NerdPack".
* Start the game and run your chosen lua unlocker.