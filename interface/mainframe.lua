local Tittle = '|T'..NeP.Interface.Logo..':15:15|t|cff'..NeP.Interface.addonColor..NeP.Info.Name

local mainFrame = {
	buttonPadding = 2,
	buttonSize = 40,
	Buttons = {},
	usedButtons = {},
	Settings = {},
	Plugins = {}
}

-- These are the default toggles.
local function defaultToggles()
	NeP.Interface.CreateToggle('MasterToggle', '', 'Interface\\ICONS\\Ability_repair.png')
	NeP.Interface.CreateToggle('Interrupts', '', 'Interface\\ICONS\\Ability_Kick.png')
	NeP.Interface.CreateToggle('Cooldowns', '', 'Interface\\ICONS\\Achievement_BG_winAB_underXminutes.png')
	NeP.Interface.CreateToggle('AoE', '', 'Interface\\ICONS\\Ability_Druid_Starfall.png')
	NeP.Interface.CreateToggle('AutoTargets','Turn this on to enable Auto Targets.', 'Interface\\Icons\\Ability_warrior_incite.png')
end

-- These are the default Settings.
local function defaultSettings()
	NeP.Interface.CreateSetting('ObjectManager', function() NeP.Interface.ShowGUI('NePOMSettings') end)
	NeP.Interface.CreateSetting('Action Log', function() PE_ActionLog:Show() end)
end

NeP.Interface.CreateSetting = function(name, func)
	mainFrame.Settings[#mainFrame.Settings+1] = {
		name = tostring(name),
		func = func
	}
end

NeP.Interface.ResetSettings = function()
	wipe(mainFrame.Settings)
	defaultSettings()
end

NeP.Interface.CreatePlugin = function(name, func)
	mainFrame.Plugins[#mainFrame.Plugins+1] = {
		name = tostring(name),
		func = func
	}
end

NeP.Interface.CreateToggle = function(name, tooltip, icon)
	mainFrame.Buttons[#mainFrame.Buttons+1] = {
		name = tostring(name),
		tooltip = tostring(tooltip),
		icon = icon,
		func = function(self)
			self.checked = not self.checked
			self:SetChecked(self.checked)
			NeP.Config.Write('bStates_'..name, self.checked)
		end
	}
	NeP.Interface.RefreshToggles()
end

local function createButtons(num, name, txt, texture, func)
	if mainFrame.usedButtons[name] ~= nil then
		mainFrame.usedButtons[name]:Show()
	else
		local pos = (mainFrame.buttonSize*#mainFrame.Buttons)+(#mainFrame.Buttons*mainFrame.buttonPadding)-(mainFrame.buttonSize+mainFrame.buttonPadding)
		mainFrame.usedButtons[name] = CreateFrame("CheckButton", nil, NePFrame, 'ActionButtonTemplate')
		local button = mainFrame.usedButtons[name]
		button:SetPoint("LEFT", NePFrame, "TOPLEFT", pos, -(mainFrame.buttonSize+5))
		button:SetSize(mainFrame.buttonSize, mainFrame.buttonSize)
		button:SetText('')
		button:SetNormalFontObject("GameFontNormal")
		button:SetScript("OnClick", func)
		button.texture = button:CreateTexture()
		button.texture:SetTexture(texture)
		button.texture:SetAllPoints()
		local htex = button:CreateTexture()
		htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight")
		htex:SetTexCoord(0, 0.625, 0, 0.6875)
		htex:SetAllPoints()
		button:SetHighlightTexture(htex)
		button:SetPushedTexture(htex)
		button.checked = NeP.Config.Read('bStates_'..name) or false
		button:SetChecked(button.checked)
		button:SetScript("OnEnter", function(self)
			local bState = NeP.Config.Read('bStates_'..name) or false
			GameTooltip:SetOwner(self, "ANCHOR_TOP")
			GameTooltip:AddLine("|cffffffff"..name..' '..(bState and '|cff08EE00ON' or '|cffFF0000OFF').."|r")
			if txt then
				GameTooltip:AddLine(txt)
			end
			GameTooltip:Show()
		end)
		button:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
	end
end

-- Refresh Toggles
NeP.Interface.RefreshToggles = function()
	for k,v in pairs(mainFrame.Buttons) do
		createButtons( k, v.name, v.tooltip, v.icon, v.func )
	end
	NePFrame:SetSize(#mainFrame.Buttons*mainFrame.buttonSize+(mainFrame.buttonPadding*#mainFrame.Buttons), mainFrame.buttonSize+25)
	NePFrame.Tittle_Frame:SetPoint('TOP', NePFrame, 0, 0)
end

-- Reset Toggles
NeP.Interface.ResetToggles = function()
	--hide toggles
	for k,v in pairs(mainFrame.usedButtons) do
		mainFrame.usedButtons[k]:Hide()
	end
	wipe(mainFrame.Buttons)
	--Create defaults
	defaultToggles()
end

-- Wait until saved vars are loaded
function NeP.Config.CreateMainFrame()

	-- Read Saved Frame Position
	local POS_1 = NeP.Config.Read('NePFrame_POS_1') or 'TOP'
	local POS_2 = NeP.Config.Read('NePFrame_POS_2') or 0
	local POS_3 = NeP.Config.Read('NePFrame_POS_3') or 0

	--parent frame 
	NePFrame = CreateFrame("Frame", "NePFrame", UIParent)
	NePFrame:SetPoint(POS_1, POS_2, POS_3)
	NePFrame:SetSize(#mainFrame.Buttons*mainFrame.buttonSize, mainFrame.buttonSize+25)
	NePFrame:SetMovable(true)
	NePFrame:EnableMouse(true)
	NePFrame:RegisterForDrag('LeftButton')
	NePFrame:SetScript('OnDragStart', NePFrame.StartMoving)
	NePFrame:SetScript('OnDragStop', function(self)
		local from, _, to, x, y = self:GetPoint()
		self:StopMovingOrSizing()
		NeP.Config.Write('NePFrame_POS_1', from)
		NeP.Config.Write('NePFrame_POS_2', x)
		NeP.Config.Write('NePFrame_POS_3', y)
	end)
	NePFrame:SetFrameLevel(0)
	NePFrame:SetFrameStrata('HIGH')
	NePFrame:SetClampedToScreen(true)

	-- Tittle
	NePFrame.Tittle_Frame = CreateFrame("Frame", nil, NePFrame)
	--NePFrame.Tittle_Frame:SetPoint('TOP', NePFrame, 0, 0)
	NePFrame.Tittle_Frame:SetBackdrop({
		bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
		tile = true, tileSize = 16, edgeSize = 16, 
		insets = { left = 4, right = 4, top = 4, bottom = 4 }});
	NePFrame.Tittle_Frame:SetBackdropColor(0,0,0,1);
	local Tittle_Text = NePFrame.Tittle_Frame:CreateFontString(nil, "OVERLAY")
	Tittle_Text:SetPoint('TOPLEFT', NePFrame.Tittle_Frame, 6, -4)
	Tittle_Text:SetFont('Fonts\\FRIZQT__.TTF', 15)
	Tittle_Text:SetText(Tittle)
	Tittle_Text:SetTextColor(1,1,1,1)
	NePFrame.Tittle_Frame:SetSize(Tittle_Text:GetStringWidth()+33, Tittle_Text:GetStringHeight()+6)

	-- Dropdown
	local function STDR_initialize(self)
		local Spec = GetSpecialization()
		local SpecInfo = GetSpecializationInfo(Spec)
		local info = UIDropDownMenu_CreateInfo()
		
		-- Routines
		info.isTitle = 1
		info.notCheckable = 1
		info.text = 'Combat Routine:'
		UIDropDownMenu_AddButton(info)
		local routinesTable = NeP.Engine.Rotations[SpecInfo] or { ['Cant find any script...'] = '' }
		for k,v in pairs(routinesTable) do
			local k = tostring(k)
			local rState = (NeP.Config.Read('NePFrame_SlctdCR_'..SpecInfo) == k)
			info = UIDropDownMenu_CreateInfo()
			info.text = k
			info.value = k
			info.checked = rState
			info.func = function()
				self.checked = not self.checked
				NeP.Config.Write('NePFrame_SlctdCR_'..SpecInfo, k)
				NeP.Core.Print('Changed Combat Routine To: ( '..k..' )')
				NeP.Interface.ResetToggles()
				NeP.Interface.ResetSettings()
				NeP.Engine.Rotations[SpecInfo][k]['InitFunc']()
			end
			UIDropDownMenu_AddButton(info)
		end
		
		-- Settings
		info.isTitle = 1
		info.notCheckable = 1
		info.text = 'Settings:'
		UIDropDownMenu_AddButton(info)
		local settingsTable = mainFrame.Settings or { ['Cant find any Setting...'] = '' }
		for k,v in pairs(settingsTable) do
			info = UIDropDownMenu_CreateInfo()
			info.text = v.name
			info.value = v.name
			info.func = v.func
			info.notCheckable = 1
			UIDropDownMenu_AddButton(info)
		end

		-- Plugins
		info.isTitle = 1
		info.notCheckable = 1
		info.text = 'Plugins:'
		UIDropDownMenu_AddButton(info)
		local pluginsTable = mainFrame.Plugins or { ['Cant find any Plugin...'] = '' }
		for k,v in pairs(pluginsTable) do
			info = UIDropDownMenu_CreateInfo()
			info.text = v.name
			info.value = v.name
			info.func = v.func
			info.notCheckable = 1
			UIDropDownMenu_AddButton(info)
		end

	end
	local function STDR_onClick(self, button)
		local ST_Dropdown = CreateFrame("Frame", "ST_Dropdown", self, "UIDropDownMenuTemplate");
		UIDropDownMenu_Initialize(ST_Dropdown, STDR_initialize, "MENU");
		ToggleDropDownMenu(1, nil, ST_Dropdown, self, 0, 0);
	end
	local ST_DropdownButtom = CreateFrame("Button", nil, NePFrame.Tittle_Frame)
	ST_DropdownButtom:SetPoint("RIGHT", NePFrame.Tittle_Frame, 0, 0)
	ST_DropdownButtom:SetSize(25, Tittle_Text:GetStringHeight()+6)
	ST_DropdownButtom:SetText('|cffFFFFFFST')
	ST_DropdownButtom:SetNormalFontObject("GameFontNormal")
	ST_DropdownButtom:SetScript("OnClick", func) 
	ST_DropdownButtom:SetBackdrop({
		bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
		edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
		tile = true, tileSize = 16, edgeSize = 16, 
		insets = { left = 4, right = 4, top = 4, bottom = 4 }});
	ST_DropdownButtom:SetBackdropColor(0,0,0,1); 
	local htex = ST_DropdownButtom:CreateTexture()
	htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight")
	htex:SetTexCoord(0, 0.625, 0, 0.6875)
	htex:SetAllPoints()
	ST_DropdownButtom:SetNormalTexture(ntex) 
	ST_DropdownButtom:SetHighlightTexture(htex)
	ST_DropdownButtom:SetPushedTexture(htex)
	ST_DropdownButtom:RegisterForClicks('anyUp')
	ST_DropdownButtom:RegisterForDrag('LeftButton', 'RightButton')
	ST_DropdownButtom:SetScript('OnClick', STDR_onClick)
	ST_DropdownButtom:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_TOP")
		GameTooltip:AddLine("|cffffffffSettings|r")
		GameTooltip:Show()
	end)
	ST_DropdownButtom:SetScript("OnLeave", function(self) GameTooltip:Hide() end)

	-- Create default Settings
	defaultSettings()

	-- Create default Toggles
	defaultToggles()

end