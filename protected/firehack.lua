function NeP.Protected.FireHack()
	if FireHack then

		NeP.Protected.Unlocker = 'FireHack'
		NeP.Core.Print('Found FireHack')

		-- Cast on ground
		function NeP.Protected.CastGround(spell, target)
			NeP.Protected.Cast(spell, target)
			CastAtPosition(ObjectPosition(target))
			CancelPendingSpell()
		end

		-- Cast
		function NeP.Protected.Cast(spell, target)
			if type(spell) == "number" then
				CastSpellByID(spell, target)
			else
				CastSpellByName(spell, target)
			end
		end

		-- Macro
		function NeP.Protected.Macro(text)
			return RunMacroText(text)
		end

		function NeP.Protected.UseItem(name, target)
			return UseItemByName(name, target)
        end

        function NeP.Protected.UseInvItem(slot)
			return UseInventoryItem(slot)
        end

		-- Distance
		function NeP.Protected.Distance(a, b)
			if ObjectExists(a) and ObjectExists(b) then
				local ax, ay, az = ObjectPosition(b)
				local bx, by, bz = ObjectPosition(a)
				return math.sqrt(((bx-ax)^2) + ((by-ay)^2) + ((bz-az)^2))
			end
			return 0
		end

		-- Infront
		function NeP.Protected.Infront(a, b)
			if ObjectExists(a) and ObjectExists(b) then
				local aX, aY, aZ = ObjectPosition(b)
				local bX, bY, bZ = ObjectPosition(a)
				local playerFacing = GetPlayerFacing()
				local facing = math.atan2(bY - aY, bX - aX) % 6.2831853071796
				return math.abs(math.deg(math.abs(playerFacing - (facing)))-180) < 90
			end
		end

		-- Firehack OM
		function NeP.OM.Maker()
			local totalObjects = ObjectCount()
			for i=1, totalObjects do
				local Obj = ObjectWithIndex(i)
				if UnitGUID(Obj) ~= nil and ObjectExists(Obj) then
					if ObjectIsType(Obj, ObjectTypes.Unit) or ObjectIsType(Obj, ObjectTypes.GameObject) then
						if NeP.Protected.Distance('player', Obj) <= 100 then
							NeP.OM.addToOM(Obj)
						end
					end
				end
			end
		end

	end
end