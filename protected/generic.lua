NeP.Protected.generic_check = false
pcall(RunMacroText, "/run NeP.Protected.generic_check = true")

function NeP.Protected.Generic()
	if NeP.Protected.generic_check then

		NeP.Protected.Unlocker = 'Generic'
		NeP.Core.Print('Found Generic Unlocker')

		-- cast on ground
		function NeP.Protected.CastGround(spell, target)
			local stickyValue = GetCVar("deselectOnClick")
			SetCVar("deselectOnClick", "0")
			CameraOrSelectOrMoveStart(1)
			Cast(spell)
			CameraOrSelectOrMoveStop(1)
			SetCVar("deselectOnClick", "1")
			SetCVar("deselectOnClick", stickyValue)
		end

		-- Cast
		function NeP.Protected.Cast(spell, target)
			if type(spell) == "number" then
				CastSpellByID(spell, target)
			else
				CastSpellByName(spell, target)
			end
		end

		-- Macro
		function NeP.Protected.Macro(text)
			RunMacroText(text)
		end

		function NeP.Protected.UseItem(name, target)
			return UseItemByName(name, target)
        end

        function NeP.Protected.UseInvItem(slot)
			return UseInventoryItem(slot)
        end

	end
end