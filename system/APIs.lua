NeP.APIs = {}

local LibDispellable = LibStub("LibDispellable-1.0")

-- Heal Engine 
local Roles = {
	['TANK'] = 20,
	['HEALER'] = 10,
	['DAMAGER'] = 1,
	['NONE'] = 1	 
}
NeP.APIs['lowest'] = function()
	local tempTable = {}	
	for i=1,#NeP.OM.unitFriend do
		local Obj = NeP.OM.unitFriend[i]
		if UnitPlayerOrPetInParty(Obj.key)
		and Obj.distance <= 40 then
			local Role = UnitGroupRolesAssigned(Obj.key) or 'NONE'
			local missingHealth = UnitHealthMax(Obj.key) - UnitHealth(Obj.key)
			local prio = Roles[tostring(Role)] * missingHealth
			tempTable[#tempTable+1] = {
				health = Obj.health,
				key = Obj.key,
				prio = prio,
				role = Role
			}
		end
	end
	table.sort(tempTable, function(a,b) return a.prio > b.prio end)
	if tempTable[1] ~= nil then
		return tempTable[1].key 
	else
		return 'player'
	end
end

NeP.APIs['AoEHeal'] = function(health)
	local health = tonumber(health)
	local tempTable = 0	
	for i=1,#NeP.OM.unitFriend do
		local Obj = NeP.OM.unitFriend[i]
		if Obj.health < health then
			tempTable = tempTable + 1
		end
	end
	return tempTable
end

-- Tank
NeP.APIs['tank'] = function()
	local tempTable = {}
	for i=1,#NeP.OM.unitFriend do
		local Obj = NeP.OM.unitFriend[i]
		local Role = UnitGroupRolesAssigned(Obj.key) or 'NONE'
		local health = UnitHealthMax(Obj.key)
		local prio = Roles[tostring(Role)] * health
		tempTable[#tempTable+1] = {
			key = Obj.key,
			prio = prio
		}
	end
	table.sort(tempTable, function(a,b) return a.prio > b.prio end)
	if tempTable[1] ~= nil then
		return tempTable[1].key 
	else
		return 'player'
	end
end

NeP.APIs['DispellAll'] = function(spell)
	for i=1,#NeP.OM.unitFriend do
		local Obj = NeP.OM.unitFriend[i]
		if LibDispellable:CanDispelWith(Obj.key, GetSpellID(GetSpellName(spell))) then
			return true, Obj.key
		end
	end
	return false, nil
end

NeP.APIs['UnitBuff'] = function(target, spell, owner)
    local buff, count, caster, expires, spellID
    if tonumber(spell) then
	    local i = 0; local go = true
	    while i <= 40 and go do
	        i = i + 1
	        buff,_,_,count,_,_,expires,caster,_,_,spellID = _G['UnitBuff'](target, i)
	        if not owner then
	        	if spellID == tonumber(spell) and caster == "player" then go = false end
	        elseif owner == "any" then
	        	if spellID == tonumber(spell) then go = false end
	        end
	    end
    else
    	buff,_,_,count,_,_,expires,caster = _G['UnitBuff'](target, spell)
    end
    return buff, count, expires, caster
end

NeP.APIs['UnitDebuff'] = function(target, spell, owner)
    local debuff, count, caster, expires, spellID
    if tonumber(spell) then
	    local i = 0; local go = true
	    while i <= 40 and go do
	        i = i + 1
	        debuff,_,_,count,_,_,expires,caster,_,_,spellID,_,_,_,power = _G['UnitDebuff'](target, i)
	        if not owner then
	        	if spellID == tonumber(spell) and caster == "player" then go = false end
	        elseif owner == "any" then
	       		if spellID == tonumber(spell) then go = false end
	        end
	    end
    else
    	debuff,_,_,count,_,_,expires,caster = _G['UnitDebuff'](target, spell)
    end
    return debuff, count, expires, caster, power
end