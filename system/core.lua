NeP = {
	Info = {
		Name = 'NerdPack',
		Nick = 'NeP',
		Author = 'MrTheSoulz',
		Version = '6.2.0.1',
		Branch = 'Test',
	},
	Interface = {
		Logo = 'Interface\\AddOns\\NerdPack\\media\\logo.blp',
		addonColor = '0070DE',
		printColor = '|cffFFFFFF',
		mediaDir = 'Interface\\AddOns\\NerdPack\\media\\',
	},
	Core = {}
}

--[[-----------------------------------------------
	** Print **
DESC: Print to chat window a message, this adds a
prefix and checks if it should print.

Build By: MTS
---------------------------------------------------]]
local lastPrint = ''
local printPrefix = '|r[|cff'..NeP.Interface.addonColor..NeP.Info.Nick..'|r]: '..NeP.Interface.printColor

function NeP.Core.Print(txt)
	local text = tostring(txt)
	if text ~= lastPrint then
		print(printPrefix..text)
		lastPrint = text
	end
end

local debug = false
function NeP.Core.Debug(prefix, txt)
	if debug then
		local prefix, text = tostring(prefix), tostring(txt)
		print(printPrefix..'(DEBUG): ('..prefix..') '..text)
	end
end

function NeP.Core.Round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

local _rangeTable = {
  ['melee'] = 1.5,
  ['ranged'] = 40,
}

function NeP.Core.UnitAttackRange(unitA, unitB, _type)
  if FireHack then 
    return _rangeTable[_type] + UnitCombatReach(unitA) + UnitCombatReach(unitB)
  else
    -- Unlockers wich dont have UnitCombatReach like functions...
    return _rangeTable[_type] + 3.5
  end
end

function GetSpellID(spell)
	if type(spell) == 'number' then return spell end

	local spellID = string.match(GetSpellLink(spell) or '', 'Hspell:(%d+)|h')
	if spellID then
		return tonumber(spellID)
	end

	return false
end

function GetSpellName(spell)
	local spellID = tonumber(spell)
	if spellID then
		return GetSpellInfo(spellID)
	end

	return spell
end

function GetSpellBookIndex(spell)
  local spellName = GetSpellName(spell)
  if not spellName then return false end
  spellName = string.lower(spellName)

  for t = 1, 2 do
    local _, _, offset, numSpells = GetSpellTabInfo(t)
    local i
    for i = 1, (offset + numSpells) do
      if string.lower(GetSpellBookItemName(i, BOOKTYPE_SPELL)) == spellName then
        return i, BOOKTYPE_SPELL
      end
    end
  end

  local numFlyouts = GetNumFlyouts()
  for f = 1, numFlyouts do
    local flyoutID = GetFlyoutID(f)
    local _, _, numSlots, isKnown = GetFlyoutInfo(flyoutID)
    if isKnown and numSlots > 0 then
      for g = 1, numSlots do
        local spellID, _, isKnownSpell = GetFlyoutSlotInfo(flyoutID, g)
        local name = GetSpellName(spellID)
        if name and isKnownSpell and string.lower(GetSpellName(spellID)) == spellName then
          return spellID, nil
        end
      end
    end
  end

  local numPetSpells = HasPetSpells()
  if numPetSpells then
    for i = 1, numPetSpells do
      if string.lower(GetSpellBookItemName(i, BOOKTYPE_PET)) == spellName then
        return i, BOOKTYPE_PET
      end
    end
  end

  return false
end

function hasTalent(row, col)
  local group = GetActiveSpecGroup()
  local talentId, talentName, icon, selected, active = GetTalentInfo(row, col, group)
  return active and selected
end