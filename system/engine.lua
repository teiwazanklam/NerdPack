NeP.Engine = {
	Run = false,
	SelectedCR = nil,
	ForceTarget = nil,
	lastCast = nil
}
NeP.Protected = {
	unlocker = nil
}
NeP.Engine.Rotations = {}

-- Register CRs
function NeP.Engine.registerRotation(SpecID, CrName, InCombat, outCombat, initFunc)
	-- If SpecID Table is not created yet, create one.
	if NeP.Engine.Rotations[SpecID] == nil then NeP.Engine.Rotations[SpecID] = {} end
	-- Create CR table
	NeP.Engine.Rotations[SpecID][CrName] = { 
		[true] = InCombat,
		[false] = outCombat,
		['InitFunc'] = initFunc or (function() return end)
	}
end

--[[-----------------------------------------------
** Automated Targets **
DESC: Checks if unit can/should be targeted.

Build By: MTS
---------------------------------------------------]]
local NeP_forceTarget = {
	-- WOD DUNGEONS/RAIDS
	[75966] = 100,	-- Defiled Spirit (Shadowmoon Burial Grounds)
	[76220] = 100,	-- Blazing Trickster (Auchindoun Normal)
	[76222] = 100,	-- Rallying Banner (UBRS Black Iron Grunt)
	[76267] = 100,	-- Solar Zealot (Skyreach)
	[76518] = 100,	-- Ritual of Bones (Shadowmoon Burial Grounds)
	[77252] = 100,	-- Ore Crate (BRF Oregorger)
	[77665] = 100,	-- Iron Bomber (BRF Blackhand)
	[77891] = 100,	-- Grasping Earth (BRF Kromog)
	[77893] = 100,	-- Grasping Earth (BRF Kromog)
	[86752] = 100,	-- Stone Pillars (BRF Mythic Kromog)
	[78583] = 100,	-- Dominator Turret (BRF Iron Maidens)
	[78584] = 100,	-- Dominator Turret (BRF Iron Maidens)
	[79504] = 100,	-- Ore Crate (BRF Oregorger)
	[79511] = 100,	-- Blazing Trickster (Auchindoun Heroic)
	[81638] = 100,	-- Aqueous Globule (The Everbloom)
	[86644] = 100,	-- Ore Crate (BRF Oregorger)
	[94873] = 100,	-- Felfire Flamebelcher (HFC)
	[90432] = 100,	-- Felfire Flamebelcher (HFC)
	[95586] = 100,	-- Felfire Demolisher (HFC)
	[93851] = 100,	-- Felfire Crusher (HFC)
	[90410] = 100,	-- Felfire Crusher (HFC)
	[94840] = 100,	-- Felfire Artillery (HFC)
	[90485] = 100,	-- Felfire Artillery (HFC)
	[93435] = 100,	-- Felfire Transporter (HFC)
	[93717] = 100,	-- Volatile Firebomb (HFC)
	[188293] = 100,	-- Reinforced Firebomb (HFC)
	[94865] = 100,	-- Grasping Hand (HFC)
	[93838] = 100,	-- Grasping Hand (HFC)
	[93839] = 100,	-- Dragging Hand (HFC)
	[91368] = 100,	-- Crushing Hand (HFC)
	[94455] = 100,	-- Blademaster Jubei'thos (HFC)
	[90387] = 100,	-- Shadowy Construct (HFC)
	[90508] = 100,	-- Gorebound Construct (HFC)
	[90568] = 100,	-- Gorebound Essence (HFC)
	[94996] = 100,	-- Fragment of the Crone (HFC)
	[95656] = 100,	-- Carrion Swarm (HFC)
	[91540] = 100,	-- Illusionary Outcast (HFC)
}

local function getTargetPrio(Obj)
	local objectType, _, _, _, _, _id, _ = strsplit('-', UnitGUID(Obj))
	local ID = tonumber(_id) or '0'
	local prio = 1
	-- If its forced
	if NeP_forceTarget[tonumber(Obj)] ~= nil then
		prio = prio + NeP_forceTarget[tonumber(Obj)] 
	end
	return prio
end

local function autoTarget()
	-- If dont have a target, target is friendly or dead
	if not UnitExists('target') or UnitIsFriend('player', 'target') or UnitIsDeadOrGhost('target') then
		local setPrio = {}
		for i=1,#NeP.OM.unitEnemie do
			local Obj = NeP.OM.unitEnemie[i]
			if UnitExists(Obj.key) and Obj.distance <= 40 then
				if UnitAffectingCombat(Obj.key) or Obj.is == 'dummy' then
					setPrio[#setPrio+1] = {
					key = Obj.key,
					bonus = getTargetPrio(Obj.key),
					name = Obj.name
					}
				end
			end
		end
		table.sort(setPrio, function(a,b) return a.bonus > b.bonus end)
		if setPrio[1] ~= nil then
			NeP.Protected.Macro('/target '..setPrio[1].key)
		end
	end
end

local unitsTable = {
	['lowest'] 	= function() return NeP.APIs['lowest']() end,
	['tank'] 	= function() return NeP.APIs['tank']() 	 end,
	['nil']		= function() return 'target' 			 end
}

local function insertToLog(spell, target)
	local spellIndex, spellBook = GetSpellBookIndex(spell)
	local spellID, name, icon
	if spellBook ~= nil then
		_, spellID = GetSpellBookItemInfo(spellIndex, spellBook)
		name, _, icon, _, _, _, _, _, _ = GetSpellInfo(spellIndex, spellBook)
	else
		spellID = spellIndex
		name, _, icon, _, _, _, _, _, _ = GetSpellInfo(spellID)
	end
	NeP.ActionLog.insert('Engine Spell Cast', name, icon, UnitName(target) or "target")
end

local function Cast(spell, target)
	
	local target = tostring(target)
	local ground = false
	
	-- Allow functions/conditions to force a target
	if NeP.Engine.ForceTarget ~= nil then
		target = NeP.Engine.ForceTarget	
	end

	-- Figure out the target
	if string.sub(target, -7) == '.ground' then
		ground = true
		target = string.sub(target, 0, -8)
		if unitsTable[target] ~= nil then
			target = unitsTable[target]()
		end
	else
		if unitsTable[target] ~= nil then
			target = unitsTable[target]()
		end
	end

	-- Cast
	if UnitExists(target) then
		if ground then
			NeP.Engine.lastCast = spell
			NeP.Protected.CastGround(spell, target)
		else
			NeP.Engine.lastCast = spell
			NeP.Protected.Cast(spell, target)
		end
	end

	-- Insert in LOG
	insertToLog(spell, target)

	-- Reset forced unit
	NeP.Engine.ForceTarget = nil
end

local function castSanityCheck(spell)
	
	
	
	if UnitCastingInfo('player') == nil
	and UnitChannelInfo('player') == nil
	and not UnitIsDeadOrGhost('player') then
		
		local _type = type(spell)

		if _type == 'table' then
			return true
		elseif _type == 'function' then
			return true
		else
			local spellIndex, spellBook = GetSpellBookIndex(spell)
			if not spellIndex then return false end
			local skillType, spellId = GetSpellBookItemInfo(spellIndex, spellBook)
			if skillType == 'FUTURESPELL' or not spellId then return false end
			if IsUsableSpell(spell) then
				return GetSpellCooldown(spell) == 0
			end
		end

	end

	return false
end

local invItems = {
	['head']	= 'HeadSlot',
	['helm']	= 'HeadSlot',
	['neck']	= 'NeckSlot',
	['shoulder']	= 'ShoulderSlot',
	['shirt']	= 'ShirtSlot',
	['chest']	= 'ChestSlot',
	['belt']	= 'WaistSlot',
	['waist']	= 'WaistSlot',
	['legs']	= 'LegsSlot',
	['pants']	= 'LegsSlot',
	['feet']	= 'FeetSlot',
	['boots']	= 'FeetSlot',
	['wrist']	= 'WristSlot',
	['bracers']	= 'WristSlot',
	['gloves']	 	= 'HandsSlot',
	['hands']	= 'HandsSlot',
	['finger1']	= 'Finger0Slot',
	['finger2']	= 'Finger1Slot',
	['trinket1'] 	= 'Trinket0Slot',
	['trinket2'] 	= 'Trinket1Slot',
	['back']	= 'BackSlot',
	['cloak']	= 'BackSlot',
	['mainhand'] 	= 'MainHandSlot',
	['offhand']	= 'SecondaryHandSlot',
	['weapon']	= 'MainHandSlot',
	['weapon1']	= 'MainHandSlot',
	['weapon2']	= 'SecondaryHandSlot',
	['ranged'] 	= 'RangedSlot'
}

local SpecialTrigers = {
	-- Pause
	['pause'] = function(spell, conditons, target)
		local conditions = NeP.DSL.parse(conditons, spell)
		if conditions then
			-- DO NOTHING!
			return true
		end
		return false
	end,
	-- Cancel cast
	['!'] = function(spell, conditons, target)
		local spell = string.sub(spell, 2);
		-- Turn string to number
		if string.match(spell, "%d") then
			spell = tonumber(spell)
		end
		if castSanityCheck(spell) then
			local conditions = NeP.DSL.parse(conditons, spell)
			if conditions then
				SpellStopCasting()
				Cast(spell, target)
				return true
			end
		end
		return false
	end,
	-- Item (FIXME)
	['#'] = function(spell, conditons, target)
		--local item = string.sub(spell, 2);
		--local conditions = NeP.DSL.parse(conditons, spell)
		--if conditions then
			--if invItems[tostring(item)] ~= nil then
				--local item = invItems[tostring(item)]
				--NeP.Protected.UseInvItem(item)
				--return true
			--else
				--NeP.Protected.UseItem(UseItem, target)
				--return true
			--end
		--end
		return false
	end,
	-- Lib
	['@'] = function(spell, conditons, target)
		local conditions = NeP.DSL.parse(conditons, '')
		if conditions then
			NeP.library.parse(false, spell, '')
			return true
		end
		return false
	end,
	-- Macro
	['/'] = function(spell, conditons, target)
		local conditions = NeP.DSL.parse(conditons, spell)
		if conditions then
			NeP.Protected.Macro(spell)
			return true
		end
		return false
	end
}

-- This allows for nesting a nest inside 1k nests and more...
local function IterateNest(table)
	local tempTable = {}
	for i=1, #table do tempTable[#tempTable+1] = table[i] end
	Iterate(tempTable)
end

-- This iterates the routine table itself.
function Iterate(table)
	for i=1, #table do
		
		local line = table[i]
		local spell = line[1]
		local target = line[3]
		local _type = type(spell)

		-- Nested
		if _type == 'table' then
			if castSanityCheck(spell) then
				local conditions = NeP.DSL.parse(line[2], '')
				if conditions then
					IterateNest(spell)
				end
			end
		end

		-- Function
		if _type == 'function' then
			if castSanityCheck(spell) then
				local conditions = NeP.DSL.parse(line[2], '')
				if conditions then
					spell()
					break
				end
			end
		end

		-- Never allow Tables/funcs to pass here
		if _type ~= 'table' then
			if _type ~= 'function' then 

				-- Special trigers
				if _type == 'string' then
					if SpecialTrigers[string.sub(spell, 1, 1)] ~= nil then
						local shouldBreak = SpecialTrigers[string.sub(spell, 1, 1)](spell, line[2], target)
						if shouldBreak then break end
					end
				end
				
				-- normal cast
				if _type == 'string' or 'number' then
					-- Turn string to number
					if string.match(spell, "%d") then
						spell = tonumber(spell)
						_type = type(spell)
					end

					-- SOME SPELLS DO NOT CAST BY IDs!
					local spell = GetSpellInfo(spell) or 'ERROR'

					if castSanityCheck(spell) then
						local conditions = NeP.DSL.parse(line[2], spell)
						if conditions then
							Cast(spell, target)
							break
						end
					end

				end
			end
		end

	end
end

-- This is just a example
function NeP.Engine.Core()
	
	local Spec = GetSpecialization() or 0
	local SpecInfo = GetSpecializationInfo(Spec)
	local SlctdCR = NeP.Config.Read('NePFrame_SlctdCR_'..SpecInfo)
	local Running = NeP.Config.Read('bStates_MasterToggle')

	if Running
	and NeP.Engine.Rotations[SpecInfo] ~= nil 
	and NeP.Engine.Rotations[SpecInfo][SlctdCR] ~= nil then
		
		local SpecTable = NeP.Engine.Rotations[SpecInfo][SlctdCR]
		local InCombatCheck = UnitAffectingCombat('player')
		local table = SpecTable[InCombatCheck]

		-- PASSIVES
		if InCombatCheck and NeP.Config.Read('bStates_AutoTargets', false) then autoTarget() end

		-- Iterate the table
		Iterate(table)

	end

end

C_Timer.NewTicker(0.05, (function()
	-- Hide FaceRoll.
	NeP.FaceRoll:Hide()
	-- Try to find unlocker.
	if NeP.Protected.Unlocker == 'FaceRoll' then
		NeP.Protected.Generic()
		NeP.Protected.FireHack()
	end
	-- Run the engine.
	NeP.Engine.Core()
end), nil)